@extends('documents.base')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center mt-5">
        <form style="width:350px;" action="{{ route('documents.store') }}" method="POST" enctype="multipart/form-data">
            <img class="mb-4" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="100%"
                height="72">
            @csrf

            <div class="form-group">
                <label for="name">Document Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Enter document name"
                    required>
            </div>
            <div class="form-group">
                <label for="desc">Description</label>
                <textarea type="text" class="form-control" name="desc" id="desc" placeholder="Description" required="required"></textarea>
            </div>
            <div class="form-group">
                <label for="file">Select File</label>
                <input type="file" name="file" class="form-control-file" id="file">
            </div>
            <button class="btn btn-lg btn-primary btn-block mt-4" type="submit">Upload Document</button>
            <p class="mt-3 mb-3 text-muted text-center">&copy; 2017-2018</p>
        </form>
    </div>
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <div class="alert alert-{{ $msg }}" role="alert">{!! Session::get('alert-' . $msg) !!}</div>
            @endif
        @endforeach
    </div>
</div>

@endsection
