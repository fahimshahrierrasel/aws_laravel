@extends('documents.base')

@section('content')
<div class="container">
    <div class="row">
            <img class="mt-2" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="100%"
            height="72">
    </div>
    <div class="d-flex justify-content-center mt-2">
        <table style="width:350px;" class="table table-borderless">
            <tbody>
                <tr>
                    <td style="width:150px;">Doucment Name</td>
                    <td>{{ $document->name}}</td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>{{ $document->desc }}</td>
                </tr>
                <tr>
                    <td>Document URL</td>
                    <td><a href="{{ $document->url }}">File Link</a></td>
                </tr>
                <tr>
                    <td>Uploaded</td>
                    <td>{{ $document->created_at->diffForHumans() }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection
