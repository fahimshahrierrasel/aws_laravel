<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Session;
use App\Document;
use Webpatser\Uuid\Uuid;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('documents.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request['name'];
        $desc = $request['desc'];
        
        $document = new Document();
        $document->name = $name;
        $document->desc = $desc;
        $document->uuid = (string) Uuid::generate(4);

        // Replacing whitespace with '-' and taking lowercase
        $file_name = strtolower(str_replace(' ', '-', $name));

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $file_name_with_extension = time(). $file_name . '.' . $file->getClientOriginalExtension();;
            $filePath = 'files/' . $file_name_with_extension;
            Storage::disk('s3')->put($filePath, file_get_contents($file));
            $fileUrl = Storage::disk('s3')->url($filePath);
            $document->url = $fileUrl;
        }

        $document->save();
        $message = 'Your uploaded file url <a class="alert-link" href="'.route('documents.show', ['uuid' => $document->uuid]).'">'.route('documents.show', ['uuid' => $document->uuid]).'</a>';
        Session::flash('alert-success', $message);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $document = Document::where('uuid', $uuid)->first();
        return view('documents.show', compact('document'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
